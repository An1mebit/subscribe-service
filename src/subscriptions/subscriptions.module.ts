import { Module } from '@nestjs/common';
import { SubscriptionsResolver } from './subscriptions.resolver';
import { PubSubModule } from '../config/pubSub.module';

@Module({
  imports: [PubSubModule],
  providers: [SubscriptionsResolver],
})
export class SubscriptionsModule {}
