import { Field, ObjectType, Float } from '@nestjs/graphql';

@ObjectType()
export class Location {
  @Field(() => [Float])
  coordinates: number[];
}
