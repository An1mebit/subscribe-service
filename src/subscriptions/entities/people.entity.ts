import { Field, ObjectType, Int } from '@nestjs/graphql';
import { Location } from './location.entity';

@ObjectType()
export class People {
  @Field(() => Location)
  location: Location;

  @Field(() => String)
  dataDate: string;

  @Field(() => String)
  fio: string;

  @Field(() => Int)
  tabNum: number;

  @Field(() => String)
  username: string;
}
