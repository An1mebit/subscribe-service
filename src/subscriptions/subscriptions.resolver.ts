/* eslint-disable @typescript-eslint/no-unused-vars */
import { Args, Float, Query, Resolver, Subscription } from '@nestjs/graphql';
import { People } from './entities/people.entity';
import { PUB_SUB, PEOPLE_UPDATE } from '../config/config';
import { Inject } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';

@Resolver(() => People)
export class SubscriptionsResolver {
  constructor(@Inject(PUB_SUB) private pubSub: PubSub) {}

  @Query(() => String)
  upTime() {
    return process.uptime;
  }

  @Subscription(() => People, {
    nullable: true,
    filter(payload, variables) {
      const isInclude = variables?.tabNums
        ? variables?.tabNums?.includes(payload?.peopleUpdated?.tabNum)
        : true;
      const isNullableCoordinates = variables?.withNullableCoordinates
        ? payload?.peopleUpdated?.location?.coordinates?.includes(0)
        : false;
      return isInclude && !isNullableCoordinates;
    },
  })
  async peopleUpdated(
    @Args('tabNums', { nullable: true, type: () => [Float] })
    tabNums?: number[],
    @Args('withNullableCoordinates', { nullable: true, type: () => Boolean })
    withNullableCoordinates?: boolean
  ) {
    return this.pubSub.asyncIterator(PEOPLE_UPDATE);
  }
}
