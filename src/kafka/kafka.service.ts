import { Injectable } from '@nestjs/common';
import { EachMessagePayload } from 'kafkajs';
import type { IPeopleMessage } from './kafka';
import { People } from 'src/subscriptions/entities/people.entity';

@Injectable()
export class KafkaService {
  mapperVehicle(data: EachMessagePayload): People {
    const { message } = data;

    const peopleMessage: IPeopleMessage = JSON.parse(message.value.toString('utf8'));
    return {
      dataDate: new Date(peopleMessage.dataDate).toISOString(),
      location: {
        coordinates: [
          parseFloat(peopleMessage.location.lat),
          parseFloat(peopleMessage.location.long),
        ],
      },
      fio: peopleMessage.fio,
      tabNum: Number(message.key.toString('utf-8')),
      username: peopleMessage.username,
    };
  }
}
