export interface IPeopleMessage {
  username: string;
  tabNum: number;
  fio: string;
  dataDate: string;
  location: {
    long: string;
    lat: string;
  };
}
