import { Controller, Inject } from '@nestjs/common';
import { KafkaService } from './kafka.service';
import { Consumer, Kafka } from 'kafkajs';
import { PUB_SUB, PEOPLE_UPDATE } from '../config/config';
import { PubSub } from 'graphql-subscriptions';
import { ConfigService } from '@nestjs/config';

@Controller()
export class KafkaController {
  constructor(
    private readonly kafkaService: KafkaService,
    @Inject(PUB_SUB) private pubSub: PubSub,
    private configService: ConfigService
  ) {
    this.kafka = new Kafka({
      brokers: [this.configService.get<string>('kafkaUrl')],
      clientId: 'test',
    });
  }

  kafka: Kafka;
  consumer: Consumer;

  async onModuleInit() {
    this.consumer = this.kafka.consumer({
      groupId: this.configService.get<string>('kafkaGroupName'),
    });
    await this.consumer.subscribe({ topic: this.configService.get<string>('kafkaTopicName') });
    await this.consumer.run({
      eachMessage: async (data) => {
        const peopleUpdated = this.kafkaService.mapperVehicle(data);
        this.pubSub.publish(PEOPLE_UPDATE, {
          peopleUpdated,
        });
      },
    });
  }
}
