import { Module } from '@nestjs/common';
import { KafkaService } from './kafka.service';
import { KafkaController } from './kafka.controller';
import { PubSubModule } from '../config/pubSub.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [PubSubModule, ConfigModule],
  controllers: [KafkaController],
  providers: [KafkaService],
})
export class KafkaModule {}
