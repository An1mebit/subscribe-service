import type { ConfigProps } from './configTypes';

export const PUB_SUB = 'PUB_SUB';
export const PEOPLE_UPDATE = 'PEOPLE_UPDATE';

export const config = (): ConfigProps => ({
  kafkaUrl: process.env.KAFKA_URL,
  kafkaGroupName: process.env.KAFKA_GROUP_NAME,
  kafkaTopicName: process.env.KAFKA_TOPIC_NAME,
});
