export interface ConfigProps {
  kafkaUrl: string;
  kafkaGroupName: string;
  kafkaTopicName: string;
}
