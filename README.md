# :dizzy: Сервис по подпискам

## Принцип работы

Сервис реализован на фреймворке [NestJS](https://nestjs.com/). Контроллер [kafka](src/kafka/kafka.controller.ts) подписывается на топис и отправляет сообщение в шину событий, при получении события сервис [subscriptions](src/subscriptions/subscriptions.resolver.ts) отправляет событие по веб сокетам клиетам :eyes:.

## Технологии

Nestjs, graphql, apollo-server, graphql-ws, kafkajs

## Запуск

Необходимо в корне создать `.env` файл, шаблон лежит в [.env.template](env/.env.template)

Команды для начала работы

```
git clone https://gitlab.com/An1mebit/subscribe-service.git
npm i
npm start
```
